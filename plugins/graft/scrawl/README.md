YYGraft
========
YYGraft 是一个采用 html5 canvas 实现的一个在线涂鸦工具。
开发初衷是用来嵌入 [KNeditor](https://gitee.com/blackfox/kindeditor)
作为插件用的，后台抽出来作为独立的工具使用了。目前实现的功能有：
1. 在线绘图主功能
2. 设置笔刷大小，笔刷颜色功能
3. 设置笔触虚化功能
4. 橡皮擦功能
5. 为涂鸦添加背景图片，以及删除背景图片功能
6. 图片保存功能

插件依赖:
========
* jQuery-1.7.1以上版本

在线预览
========
### 在线演示: http://www.r9it.com/demo/yygraft/
